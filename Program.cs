﻿namespace Calculadora
{
    public class Program
    {
        static void Main(string[] args)
        {
            Menu();
        }

        static void Menu()
        {
            Console.WriteLine("Calculadora");
            Console.WriteLine("-----------");
            Console.WriteLine("Escolha uma opção:");
            Console.WriteLine("1 - Somar");
            Console.WriteLine("2 - Subtrair");
            Console.WriteLine("3 - Multiplicar");
            Console.WriteLine("4 - Dividir");
            Console.WriteLine("0 - Sair");
            short opcao = short.Parse(Console.ReadLine());

            switch (opcao)
            {
                case 1 : Soma(); break;
                case 2 : Subtracao(); break;
                case 3 : Multiplicacao(); break;
                case 4 : Divisao(); break;
                case 0 : System.Environment.Exit(0); break;
                default: Menu(); break;
            }
        }

        static void Soma()
        {
            Console.WriteLine("Digite o primeiro número: ");
            int num1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Digite o segundo número:");
            int num2 = int.Parse(Console.ReadLine());

            int soma = num1 + num2;

            Console.WriteLine("O resultado da soma é: " + soma);
            Console.ReadKey();
            Menu();
        }

        static void Subtracao()
        {
            Console.WriteLine("Digite o primeiro número: ");
            int num1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Digite o segundo número:");
            int num2 = int.Parse(Console.ReadLine());

            int subtracao = num1 - num2;

            Console.WriteLine("O resultado da soma é: " + subtracao);
            Console.ReadKey();
            Menu();
        }

        static void Multiplicacao()
        {
            Console.WriteLine("Digite o primeiro número: ");
            int num1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Digite o segundo número:");
            int num2 = int.Parse(Console.ReadLine());

            int multiplicacao = num1 * num2;

            Console.WriteLine("O resultado da soma é: " + multiplicacao);
            Console.ReadKey();
            Menu();
        }

        static void Divisao()
        {
            Console.WriteLine("Digite o primeiro número: ");
            int num1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Digite o segundo número:");
            int num2 = int.Parse(Console.ReadLine());

            int divisao = num1 / num2;

            Console.WriteLine("O resultado da soma é: " + divisao);
            Console.ReadKey();
            Menu();
        }
    }
}
